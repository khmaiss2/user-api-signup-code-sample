<?php

/**
 * Text Constants that will be consumed (Mainly Messages) 
 *
 */ 
Class TextConstants {
    const FULL_NAME_ERROR_TEXT = "Full name must contain only alphabets.";
    const USERNAME_ERROR_TEXT = "Username must contain only letters and numbers.";
    const EMAIL_ERROR_TEXT = "Email is not valid.";
    const PASSWORD_ERROR_TEXT = "Password must contain atleast 1 uppercase letter and 1 number.";
    const USER_EXIST_ERROR_TEXT = "User exist. Please try to login instead.";
}

/**
 * User Class
 *
 */ 
Class User {
    /**
     * Find user by ID.
     *
     * @param integer     $id       User id
     *
     * @return Array
     */
    public function find($id){
        //Returns user if exist, or false.
    }
     /**
     * Add user.
     *
     * @param array     $data       Associative array for field=>value to add.
     *
     * @return boolean Returns true if added, or false if not.
     */
    public function add($data){
        //hashes the password then add user to database. If the process was not successfull it will return false
    }
    /**
     * Update user.
     *
     * @param integer     $id       User ID.
     * @param array     $data       Associative array for field=>value to update.
     *
     * @return boolean Returns true if updated, or false if not.
     */
    public function update($id,$data){
        //update the user if exist. If the process was not successfull it will return false
    }
    /**
     * Delete user.
     *
     * @param integer     $id       User ID.
     *
     * @return boolean Returns true if deleted, or false if not.
     */
    public function delete($id){

    }
    /**
     * Find user by Email.
     *
     * @param string     $email       email to find.
     *
     * @return array|boolean Returns array with user data if exist, or false.
     */
    public function findByEmail($email){
        
    }
    /**
     * Find user by Username.
     *
     * @param string     $username       Username to find.
     *
     * @return array|boolean Returns array with user data if exist, or false.
     */
    public function findByUsername($username){
       
    }
}

function isValidFullName($fullName){
    //Fullname validation
    return true;
}
function isValidUsername($username){
    //Username validation
    return true;
}
function isValidEmail($email){
    //Email validation
    return true;
}
function isValidPassword($password,$repeatPassword){
    //Password validation
    return true;
}

function validateData($data){
    $errors = array();
    if(!isValidFullName($data['full_name'])){
        $errors[] = TextConstants::FULL_NAME_ERROR_TEXT;
    }
    if(!isValidUsername($data['username'])){
        $errors[] = TextConstants::USERNAME_ERROR_TEXT;
    }
    if(!isValidEmail($data['email'])){
        $errors[] = TextConstants::EMAIL_ERROR_TEXT;
    }

    if(!isValidPassword($data['password'],$data['repeat_password'])){
        $errors[] = TextConstants::PASSWORD_ERROR_TEXT;
    }
    return $errors;
}


/* Main Code */
header('Content-Type: application/json; charset=utf-8');

//Get Data
$fullName = $_POST['full_name'];
$username = $_POST['username'];
$email = $_POST['email'];
$password = $_POST['password'];
$repeatPassword = $_POST['repeat_password'];

//Data validation
$errors = validateData([
    'full_name' => $fullName,
    'username' => $username,
    'email' => $email,
    'password' => $password,
    'repeat_password' => $repeatPassword,
]);
if(sizeof($errors) > 0){
    http_response_code(400);
    echo json_encode($errors);
    die();
}

//Check if user exists then create it
$user = new User();
if(!$user->findByEmail($email) && !$user->findByUsername($username)){
    $data = [
        'full_name' => $fullName,
        'username' => $username,
        'email' => $email,
        'password' => $password,
    ];
    if($user->add($data)){
        echo json_encode($data);
        die();
    }
}else{
    http_response_code(400);
    $errors[] = TextConstants::USER_EXIST_ERROR_TEXT;
    echo json_encode($errors);
    die();
}


